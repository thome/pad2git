A pad-git background save robot
===============================

Author: Emmanuel Thomé, 2021. Contributions welcome.

This script sets up a background job that mirrors the content of an
etherpad page, and stores its evolution as independent commits in a git
repository.

Note that the mirroring work is unidirectional! There is no way to use
this projet to modify a pad using git.

Requirements
------------

To use this, you need the following:
   - access to an etherpad pad.
   - an account on a gitlab instance, and sufficient permissions to
     create/fork projects on this instance.
   - a gitlab runner that can be used in a gitlab project. (depending on
     your gitlab instance, shared runners might be available)

Beyond the requirements above, the instructions here do not assume any
fancy software to be installed on your computer.

Some familiarity with gitlab does no harm, but hopefully the instructions
below should be easy enough to follow (despite the many steps).


Setup
-----

This walkthrough assumes that you use
[gitlab.inria.fr](https://gitlab.inria.fr/) as your gitlab instance. If
you use an external gitlab instance, all links marked by the string
"*(adjust to your gitlab)*" must be changed.

1. Fork this project. You have two options:
   1. *(recommended option, more flexible)*
      - Import this project to a new project, by following this link:
        https://gitlab.inria.fr/projects/new#import_project *(adjust
        to your gitlab)*
      - Click on "Repo by URL" (last button)
      - As "Git repository URL", give https://gitlab.inria.fr/thome/pad2git
      - You don't have to specify username nor password, since you're
        copying this project, which is public.
      - Choose your project name.
      - Decide on the visibility level.
        **You most probably want a private project**, see
        [#security-considerations](security considerations) further down
        for more information.
      - Click on "Create project"
      <br />


   2. *(not recommended, only works on [gitlab.inria.fr](https://gitlab.inria.fr/), has some pitfalls, but somewhat quicker)*
      
      Go to https://gitlab.inria.fr/thome/pad2git/-/forks/new to fork
      to a project named "pad2git" in any gitlab.inria.fr namespace
      that you own.

      (Note: the fork relationship is useless here)
      
      (**Pitfall**: the fork's visibility will be the same as this
      project, which is public.  **This is most probably not what you
      want**, see [security considerations](#security-considerations)
      further down for more information.
      <br />

2. Set up the project to use gitlab "pipelines" (more info: [gitlab doc](https://docs.gitlab.com/ce/ci/pipelines/))
   - In the left navigation pane, go to "Settings", then "General"
   - Expand the "Visibility, project features, permissions" section
   - Activate "Pipelines" (only for project members is fine, and should
     be the only option anyway if yout project is private).
   - Click on "Save changes"
   <br />

3. Set up a runner for this project. (more info: [gitlab doc](https://docs.gitlab.com/ce/ci/runners/README.html))
   - In the left navigation pane, go to "CI/CD"
   - Expand the "Runners" section
   <br />

   From there on, it is a bit harder than it should be.  There are
   several possible situations.
   - If your gitlab instance has shared runners available (which
     [gitlab.inria.fr](https://gitlab.inria.fr/), at the time of this writing, does not), you can
     pick any of the shared runners that are listed on the right column.
   - If you have maintainer level rights on a project that has custom
     (unlocked) runners attached to it, you may select one of these
     runners, which should appear in the left column. (on
     [gitlab.inria.fr](https://gitlab.inria.fr/), users can ask to be
     listed as maintainers of the
     [inria-ci/qlf-ci-gitlab-runner](https://gitlab.inria.fr/inria-ci/qlf-ci-gitlab-runner)
     project, which is designed for this purpose)
   - If none of the above works, you have to setup a runner yourself.
     Take note of the registration token that is displayed in the left
     column, under the section "Set up a specific runner manually".
     Your mileage may vary. A linux machine with internet access
     (preferrably wired) is needed. On Debian GNU/Linux, this is a
     matter of installing the packages `docker.io` and `gitlab-runner`.
     Once this is done, run the following command, where
     `REGISTRATION_TOKEN` is the token that you've just looked up.
     *(adjust to your gitlab)*
     ```
     sudo gitlab-runner register --non-interactive -u https://gitlab.inria.fr/ -r REGISTRATION_TOKEN --executor docker
     ```
     Once this is done, you should be in the situation of the item
     above, with a selectable runner on the left.
    <br />

4. Set up a long job timeout for this project. (more info: [gitlab doc](https://docs.gitlab.com/ce/ci/pipelines/settings#timeout))
   - In the left navigation pane, go to "CI/CD"
   - Expand the "General pipelines" section
   - Leave most settings unchanged, and set the "Timeout" value to the
     maximum time you expect your mirroring task to live. This can
     easily be counted in hours.
   - Click on "Save changes"
   <br />

5. Create a project access token (more info: [gitlab doc](https://docs.gitlab.com/ce/user/project/settings/project_access_tokens.html))
   - In the left navigation pane, go to "Access Tokens"
   - Choose a name. This will be the name under which all pad edits will
     appear. A name like "bot" is probably fine.
   - Optionally, set up an expiration date.
   - Select the "api" scope. 
   - Click on "Create project access token"
   - Remember the token, since you will need it later on.
   <br />

6. *(optional and not really recommended, see [security
   considerations](#security-considerations))* Update the contents of the .gitlab-ci.yml with the
   `PAD`, `PAD_PASSWORD`, and `PROJECT_ACCESS_TOKEN` variables. Seasoned
   users will do this with a git clone and a commit, but alternatively
   this can also be done from the web interface:
   - In the left navigation pane, go to "Project overview" (on top)
   - Click on the "CI/CD configuration" box
   - Click on the "Edit" button
   - Adjust `PAD`, `PAD_PASSWORD`, and `PROJECT_ACCESS_TOKEN`.
   - Click on "Commit changes"
   <br />

Congratulations. At this point you should be ready to start a mirroring
job!


Start a mirroring job
---------------------

This project arranges so that mirroring jobs are run as gitlab pipelines.

There's more than one way to start a mirroring job.

The recommended method, which does not rely on any software on your
computer, is to start a pipeline from the gitlab web interface. To do
that:
   - In the left navigation pane, go to "CI/CD", then "Pipelines"
   - Click on the "Run Pipeline" button (near top right)
   - If you've done the optional step 6 above, skip to the following
     step.  Otherwise, set variable names and values as follows:
     - PAD: set this to the address of the pad you want to mirror, e.g.
       https://pad.inria.fr/p/adasjdjkashdjakfdg
     - PROJECT_ACCESS_TOKEN: set this to the project access token that
       you created at step 5 above
     - PAD_PASSWORD (optional): set this to the password of your pad, if your pad
       has a password.
     - MAXTIME (optional): number of seconds that you want your mirroring
       job to run. Defaults to the value in
       [`.gitlab-ci.yml`](.gitlab-ci.yml)
     - INTERVAL (optional): number of seconds between two checks of the
       pad contents.  Defaults to the value in
       [`.gitlab-ci.yml`](.gitlab-ci.yml)
   - click "Run Pipeline" again (bottom left).

Alternatively, if you have the required software tools (basically
a standard python interpreter, really), the second method is to use the
`pad2git.py` script as follows from your computer in order to trigger a
new pipeline *(adjust to your gitlab)*:
```
./pad2git.py -s $INTERVAL -t $MAXTIME -p $PAD -T $PROJECT_ACCESS_TOKEN -P $PAD_PASSWORD --gitlabapi https://gitlab.inria.fr/api/v4 --gitlabproject $YOUR_PROJECT_PATH --trigger
```
(where all variables must be substituted according to their description
with the previous method).


The third method is to pick any of the previously created pipelines, and
restart (retry) the corresponding jobs. As long as you haven't touched
the script itself, it should work just fine.


Output of a mirroring job
-------------------------

A mirroring job runs for the amount of time that is specified by the
`MAXTIME` variable in [`.gitlab-ci.yml`](.gitlab-ci.yml), or provided
among the variables that you specified when creating the pipeline.

Every `INTERVAL` seconds (again, from [`.gitlab-ci.yml`](.gitlab-ci.yml)
or from the pipeline variables), the script checks for changes in the
pad, and updates the `pad.html` file in the repository by creating a new
commit. If the file does not exist, it is created.


Using gitlab.com since it has shared runners
--------------------------------------------

[gitlab.com](https://gitlab.com/) has shared runners, but the [free
tier](https://about.gitlab.com/pricing/) of the pricing plans has a limit
of 400 runner minutes per month. This means that you won't be able to
record more than 6 hours of meeting per month, which is not much.


Security considerations
-----------------------

This script deals with two pieces of sensitive data:
   - your project access token
   - the address to your pad and, optionally, its password.

Because of this sensitive data, it is not recommended to make your
project public, and it is not recommended to stick this information in
[`.gitlab-ci.yml`](.gitlab-ci.yml). (You really, really must not do both!)

On the other hand, having them as pipeline variables should be OK, as
long as their values do not appear in the pipeline job transcripts.


It is probably wise to set up your project access token with an
expiration date.


Viewing the changes from the git history
----------------------------------------

If you try to view the changes with `git log -p` from a checkout of the
repository, you might be deceived since the html format is not well
adapted to line-based diff tools.  We have a helper script that can be
useful. For this, you need to have the `xmllint` and `xmldiff` tools
installed (both from packages of the same name under Debian GNU/Linux.
Use as follows:

```
GIT_EXTERNAL_DIFF=./diffhtml.sh git log -p --ext-diff pad.html
```

Further questions
-----------------

Q: is it possible to do without the api scope for the project access
token? <br />
A: not that I'm aware of, but you might try and see if the permission
level can be lowered.
<br />

Q: doesn't etherpad have a REST API that you can use? <br />
A: yes, but pad.inria.fr seems to hide it. Feel free to improve this
script if it so happens that the REST API can be accessed. I agree that
it should still be accessible, but I did not succeed thus far.
<br />

Q: is it possible to render `pad.html` in plain text? <br />
A: yes, probably, but this is not done. Patches welcome. The easiest
route is probably to render the html from your browser.
<br />

Q: is it possible to make each change an individual commit? <br />
A: most probably not, at least not in the way the script works currently.
And anyway, the notion of "individual change" on etherpad is at the
character level, which would mean many, many commits.
<br />

Q: I don't want to use gitlab pipelines and runners. Is it possible?<br
/>
A: Yes. You need some software, though. The `python3-selenium` and
`chromium-driver`, most certainly. Once you have that, you should be
able to run the following command *(adjust to your gitlab)*:
```
./pad2git.py -s $INTERVAL -t $MAXTIME -p $PAD -T $PROJECT_ACCESS_TOKEN -P $PAD_PASSWORD --gitlabapi https://gitlab.inria.fr/api/v4 --gitlabproject $YOUR_PROJECT_PATH
```
<br />

Q: This runs for more than the `maxtime` setting!<br />
A: Yes, there is some minor drift.
<br />


Q: How is concurrency handled?<br />
A: Not well. Basically, `pad.html` is overwritten every time. It's not a
very subtle approach, admittedly.
<br />

